#!/bin/bash
# Script for installing tmux on systems where you don't have root access.
# tmux will be installed in $HOME/local/bin.
# It's assumed that wget and a C/C++ compiler are installed.
# exit on error
set -e
TMUX_VERSION=1.6
# create our directories
# extract files, configure, and compile
############
# libevent #
############
tar xvzf libevent-2.0.19-stable.tar.gz
cd libevent-2.0.19-stable
chmod +x configure
./configure --prefix=$HOME/local --disable-shared
make
make install
cd ..
############
# ncurses #
############
tar xvzf ncurses-5.9.tar.gz
cd ncurses-5.9
chmod +x configure
./configure --prefix=$HOME/local
make
make install
cd ..
############
# tmux #
############
tar xvzf tmux-${TMUX_VERSION}.tar.gz
cd tmux-${TMUX_VERSION}
chmod +x configure
./configure CFLAGS="-I$HOME/local/include -I$HOME/local/include/ncurses" LDFLAGS="-L$HOME/local/lib -L$HOME/local/include/ncurses -L$HOME/local/include"
CPPFLAGS="-I$HOME/local/include -I$HOME/local/include/ncurses" LDFLAGS="-static -L$HOME/local/include -L$HOME/local/include/ncurses -L$HOME/local/lib" make
cp tmux $HOME/local/bin
cd ..
# cleanup
rm -rf $HOME/tmux_tmp
echo "$HOME/local/bin/tmux is now available. You can optionally add $HOME/local/bin to your PATH."
